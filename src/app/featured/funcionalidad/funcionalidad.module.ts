import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { FuncionalidadRoutingModule } from './funcionalidad-routing.module';
import { NativeScriptCommonModule } from '@nativescript/angular';


@NgModule({
  declarations: [],
  imports: [
    FuncionalidadRoutingModule,
    NativeScriptCommonModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class FuncionalidadModule { }
